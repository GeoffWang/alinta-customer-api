package com.alinta.api.customerapi.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.alinta.api.customerapi.CustomerApiApplication;
import com.alinta.api.customerapi.model.Customer;
import com.alinta.api.customerapi.service.CustomerService;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = CustomerApiApplication.class)
@AutoConfigureMockMvc
public class CustomerControllerIntegrationTest {
	@Autowired
    MockMvc mvc;
	
	@Autowired
	CustomerService custService;
	
	@Test
	public void testSuccessfulCreate() throws Exception {
		mvc.perform(post("/api/v1/customers").content("{\"firstName\":\"Tony\",\"lastName\":\"Parker\",\"dateOfBirth\":\"1982-05-17\"}").contentType(MediaType.APPLICATION_JSON));

		// Query DB for new customer.
		List<Customer> customers = custService.getCustomerByNamePartial("park");
		// Should only be 1 match.
		assertTrue(customers.size() == 1);
		
		assertEquals(LocalDate.of(1982, 5, 17), customers.get(0).getDateOfBirth());
	}
	
	@Test
	public void testSuccessfulUpdate() throws Exception {
		mvc.perform(patch("/api/v1/customers/5").content("{\"lastName\":\"Apple\"}").contentType(MediaType.APPLICATION_JSON));

		// Query DB for updated customer and check last name changed.
		assertEquals("Apple", custService.getCustomerById(5l).getLastName());
	}
	
	@Test
	public void testSuccessfulDelete() throws Exception {
		mvc.perform(delete("/api/v1/customers/1"));
		
		// We should get a not found exception when we try to retrieve customer by ID again.
		Exception exception = assertThrows(EntityNotFoundException.class, () -> {
	        custService.getCustomerById(1l);
	    });
		
		assertEquals("Customer with ID 1 not found.", exception.getMessage());
	}
}
