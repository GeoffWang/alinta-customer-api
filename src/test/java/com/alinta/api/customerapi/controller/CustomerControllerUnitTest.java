package com.alinta.api.customerapi.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;

import com.alinta.api.customerapi.CustomerApiApplication;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.MOCK, classes = CustomerApiApplication.class)
@AutoConfigureMockMvc
public class CustomerControllerUnitTest {
	@Autowired
    MockMvc mvc;
	
	@Test
	public void testGetCustomersByFirstOrLastNamePartial() throws Exception {
		// Queries the partial name API and checks whether response has the two expected customers.
		mvc.perform(get("/api/v1/customers?partialName=murray").contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
        	.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        	.andExpect(jsonPath("$", hasSize(2)))
        	.andExpect(jsonPath("$[0].firstName", is("Dejounte")))
        	.andExpect(jsonPath("$[1].firstName", is("Jamal")));
	}
	
	@Test
	public void testMissingNameGetCustomersByFirstOrLastNamePartial() throws Exception {
		// Use the wrong query param.
		mvc.perform(get("/api/v1/customers?somethingelse=murray").contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isBadRequest())
		    .andExpect(result -> assertEquals("{\"status\":\"BAD_REQUEST\",\"message\":\"Required request parameter 'partialName' for method parameter type String is not present\"}", 
					result.getResponse().getContentAsString()));
	}
	
	@Test
	public void testSuccessfulCreate() throws Exception {
		mvc.perform(post("/api/v1/customers").content(
				"{\"firstName\":\"Tony\",\"lastName\":\"Parker\",\"dateOfBirth\":\"1982-05-17\"}").contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
        	.andExpect(jsonPath("$.firstName", is("Tony")));
	}
	
	@Test
	public void testCreateWithIncorrectDate() throws Exception {
		// Use the wrong date format.
		mvc.perform(post("/api/v1/customers").content(
				"{\"firstName\":\"Tony\",\"lastName\":\"Parker\",\"dateOfBirth\":\"17-05-1982\"}").contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isBadRequest())
			.andExpect(result -> assertEquals("{\"status\":\"BAD_REQUEST\",\"message\":\"Please check the format of the date is yyyy-mm-dd. Text '17-05-1982' could not be parsed at index 0\"}", 
					result.getResponse().getContentAsString()));
	}
	
	@Test
	public void testCreateMissingAttribute() throws Exception {
		// Does not have last name.
		mvc.perform(post("/api/v1/customers").content(
				"{\"firstName\":\"Tony\",\"dateOfBirth\":\"1982-05-17\"}").contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isBadRequest())
			.andExpect(result -> assertEquals("{\"status\":\"BAD_REQUEST\",\"message\":\"[Last name must not be empty]\"}", result.getResponse().getContentAsString()));
	}
	
	@Test
	public void testSuccessfulUpdate() throws Exception {
		mvc.perform(patch("/api/v1/customers/5").content(
				"{\"lastName\":\"Apple\"}").contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
        	.andExpect(jsonPath("$.lastName", is("Apple")));
	}
	
	@Test
	public void testUpdateIncorrectDate() throws Exception {
		// Use the wrong date format.
		mvc.perform(patch("/api/v1/customers/5").content(
				"{\"dateOfBirth\":\"17-05-1982\"}").contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isBadRequest())
			.andExpect(result -> assertEquals("{\"status\":\"BAD_REQUEST\",\"message\":\"Value 17-05-1982 of attribute com.alinta.api.customerapi.model.Customer[\\\"dateOfBirth\\\"] is invalid.\"}", 
					result.getResponse().getContentAsString()));
	}
	
	@Test
	public void testUpdateNonExistentId() throws Exception {
		// Use a customer Id that doesn't exist.
		mvc.perform(patch("/api/v1/customers/0").content(
				"{\"lastName\":\"Apple\"}").contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isNotFound())
			.andExpect(result -> assertEquals("{\"status\":\"NOT_FOUND\",\"message\":\"Customer with ID 0 not found.\"}", result.getResponse().getContentAsString()));
	}
	
	@Test
	public void testUpdateNothing() throws Exception {
		// Update an attribute that doesn't exist.
		mvc.perform(patch("/api/v1/customers/5").content(
				"{\"middleName\":\"Apple\"}").contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
        	.andExpect(jsonPath("$.lastName", is("Strawberry")));
	}
	
	@Test
	public void testSuccessfulDelete() throws Exception {
		mvc.perform(delete("/api/v1/customers/1").contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isNoContent());
	}
	
	@Test
	public void testDeleteNonExistentId() throws Exception {
		// Delete a customer that doesn't exist.
		// This will have the same result as a successful delete since it always return 204.
		mvc.perform(delete("/api/v1/customers/0").contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isNoContent());
	}
}
