package com.alinta.api.customerapi.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.alinta.api.customerapi.model.Customer;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class CustomerServiceTest {

	@Autowired
	CustomerService custService;
	
	@Test
	public void testGetCustomerByNamePartial() {
		List<Customer> allTims = custService.getCustomerByNamePartial("tIm");
		// Two people with first name Tim.
		assertEquals(2, allTims.size());
		
		List<Customer> allDuncs = custService.getCustomerByNamePartial("dunc");
		// Only one person with last name containing dunc.
		assertEquals(1, allDuncs.size());
	}
	
	@Test
	public void testCreateCustomer() {
		String firstName = "Manu";
		String lastName = "Ginobili";
		
		Customer createdCust = custService.createCustomer(new Customer(firstName, lastName, LocalDate.of(1977, 7, 28)));
		
		// Query DB with newly created customer's ID to see if it exists.
		Customer retrievedCust = custService.getCustomerById(createdCust.getId());
		
		assertEquals(firstName, retrievedCust.getFirstName());
		assertEquals(lastName, retrievedCust.getLastName());
		assertEquals(LocalDate.of(1977, 7, 28), retrievedCust.getDateOfBirth());
	}
	
	@Test
	public void testUpdateCustomer() {
		Long customerId = 5l;
		Customer cust = custService.getCustomerById(customerId);
		
		// Change first name.
		String newFirstName = "DJ";
		cust.setFirstName(newFirstName);
		custService.updateCustomer(cust);
		
		// Query DB again.
		Customer retrievedCust = custService.getCustomerById(customerId);
		
		// Check first name changed
		assertEquals(newFirstName, retrievedCust.getFirstName());
	}
	
	@Test
	public void testDeleteCustomer() {
		Long customerId = 2l;
		custService.deleteCustomer(customerId);
		
		// We should get a not found exception when we try to retrieve customer by ID again.
		Exception exception = assertThrows(EntityNotFoundException.class, () -> {
	        custService.getCustomerById(customerId);
	    });

	    assertTrue(exception.getMessage().equals("Customer with ID 2 not found."));
	}
}
