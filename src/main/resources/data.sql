INSERT INTO customer (id, first_name, last_name, date_of_birth) values(1, 'Tim', 'Duncan', parseDateTime('1976-04-25','yyyy-MM-dd'));
INSERT INTO customer (id, first_name, last_name, date_of_birth) values(2, 'Dejounte', 'Murray', parseDateTime('1996-09-19','yyyy-MM-dd'));
INSERT INTO customer (id, first_name, last_name, date_of_birth) values(3, 'Jamal', 'Murray', parseDateTime('1997-02-23','yyyy-MM-dd'));
INSERT INTO customer (id, first_name, last_name, date_of_birth) values(4, 'Tim', 'Thomas', parseDateTime('1977-02-26','yyyy-MM-dd'));
INSERT INTO customer (id, first_name, last_name, date_of_birth) values(5, 'Darryl', 'Strawberry', parseDateTime('1985-06-15','yyyy-MM-dd'));