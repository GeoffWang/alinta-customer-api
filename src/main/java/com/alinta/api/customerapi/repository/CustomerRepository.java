package com.alinta.api.customerapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.alinta.api.customerapi.model.Customer;

/**
 * The customer JPA repository interface. Can define new methods here for different queries.
 * @author Geoff Wang
 *
 */
@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
	/**
	 * Find all customers with first name containing a specified keyword and a last name containing a keyword. In the customer service, we use the same keyword for first and last name.
	 * @param firstName the keyword that the first name should contain.
	 * @param lastName the keyword that the last name should contain.
	 * @return list of customers which have a first name containing the firstName value or a last name containing the lastName value.
	 */
	List<Customer> findByFirstNameContainingOrLastNameContainingAllIgnoreCase(String firstName, 
			String lastName);
}
