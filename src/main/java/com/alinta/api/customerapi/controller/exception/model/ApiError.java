package com.alinta.api.customerapi.controller.exception.model;

import org.springframework.http.HttpStatus;

/**
 * Class for returning errors in the controller in JSON format.
 * @author Geoff Wang
 *
 */
public class ApiError {
	private HttpStatus status;
	private String message;
	
	public ApiError(HttpStatus status, String message) {
		this.status = status;
		this.message = message;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
