package com.alinta.api.customerapi.controller.exception;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.alinta.api.customerapi.controller.exception.model.ApiError;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;

/**
 * Exception handler class to handle the processing of exceptions that occur in the controller and returns adequate error details in JSON format.
 * @author Geoff Wang
 *
 */
@ControllerAdvice
public class CustomerControllerExceptionHandler extends ResponseEntityExceptionHandler {
	public CustomerControllerExceptionHandler() {
        super();
    }
	
	/**
	 * Handles ConstraintViolationException exceptions, expected to occur when not all required attributes are provided.
	 * @param ex the constraint violation exception.
	 * @param request contains some details about the request.
	 * @return a JSON object with an error message containing all the violations that occurred and HTTP code.
	 */
	@ExceptionHandler({ ConstraintViolationException.class })
	@ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<Object> handleConstraintViolations(final ConstraintViolationException ex, final WebRequest request) {
		List<String> violations = ex.getConstraintViolations().stream().map(v -> v.getMessage()).collect(Collectors.toList());
        
		ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, violations.toString());
		
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
    }
	
	/**
	 * Handles EntityNotFoundException exceptions, expected to occur when entity requested is not found in the database. 
	 * @param ex the entity not found exception.
	 * @param request contains some details about the request.
	 * @returns JSON object with an error message and HTTP code.
	 */
	@ExceptionHandler({ EntityNotFoundException.class })
	@ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<Object> handleEntityNotFound(final EntityNotFoundException ex, final WebRequest request) {        
		ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, ex.getMessage());
		
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
    }
	
	/**
	 * Handles JsonMappingException exceptions, not really expected to occur but here because its thrown by Jackson ObjectMapper.
	 * @param ex the JSON mapping exception.
	 * @param request contains some details about the request.
	 * @return JSON object with an error message and HTTP code.
	 */
	@ExceptionHandler({ JsonMappingException.class })
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<Object> handleJsonMappingException(final JsonMappingException ex, final WebRequest request) {        
		ApiError apiError = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
		
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
    }
	
	/**
	 * Handles InvalidFormatException exceptions, will occur when updating a customer and there is invalid input.
	 * @param ex the invalid format exception.
	 * @param request contains some details about the request.
	 * @return JSON object with an error message that lists the value with the incorrect format and the attribute it belongs to along with a HTTP code.
	 */
	@ExceptionHandler({ InvalidFormatException.class })
	@ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<Object> handleInvalidFormat(final InvalidFormatException ex, final WebRequest request) {
		String errorMessage = "Value " + ex.getValue().toString() + " of attribute " + ex.getPathReference() + " is invalid.";
		// Add some more specific messaging around date format as that is the most likely to trip someone up.
		if (ex.getTargetType().equals(LocalDate.class)) {
			errorMessage.concat(" Please check the format of the date is yyyy-mm-dd.");
		}
		ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, errorMessage);
		
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
    }
	
	// This will occur when we try to pass in a Customer object (create new customer) and it is invalid in some way.
	// This one already exists so we need to override it.
	@Override
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		String errorMessage = "";
		// Add some more specific messaging around date format as that is the most likely to trip someone up.
		if (ex.getMostSpecificCause().getClass().equals(DateTimeParseException.class)) {
			errorMessage = "Please check the format of the date is yyyy-mm-dd. ";
		}
		
		ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, errorMessage.concat(ex.getMostSpecificCause().getMessage()));
		return handleExceptionInternal(ex, apiError, headers, status, request);
	}
	
	// This will usually occur when we are missing query or request parameters.
	// This one already exists so we need to override it.
	@Override
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ex.getMessage());
		
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
	}
}
