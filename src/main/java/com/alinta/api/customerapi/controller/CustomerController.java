package com.alinta.api.customerapi.controller;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.alinta.api.customerapi.model.Customer;
import com.alinta.api.customerapi.service.CustomerService;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

/***
 * The customer API rest controller. Maps rest API endpoints with the functionality required for each. Calls the service class when it needs to interact with the back end.
 * @author Geoff Wang
 *
 */
@Tag(name = "Customer API", description = "Handles all operations related to customers")
@RestController
@RequestMapping(value = {"/api/v1/customers"})
public class CustomerController {
	@Autowired
	CustomerService custService;
	
	@Autowired
	ObjectMapper objectMapper;
	
	/**
	 * Find all customers with first or last name containing the partial name provided.
	 * @param partialName The keyword that the first or name should contain
	 * @return list of customers where first or last name contain the partial name input provided.
	 */
	@Operation(summary = "Find all customers with first or last name containing the partial name provided")
	@ApiResponse(responseCode = "200", 
					description = "Returns list of customers where first or last name contain the partial name input provided", 
					content = {@Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Customer.class)))}
	)
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Customer>> getCustomersByFirstOrLastNamePartial(
			@Parameter(description="The keyword that the first or name should contain") @Valid @RequestParam String partialName) {
		return ResponseEntity.ok(custService.getCustomerByNamePartial(partialName));
	}
	
	/**
	 * Create a new customer.
	 * @param newCustomer
	 * @return the newly created customer
	 */
	@Operation(summary = "Create a new customer")
	@ApiResponses(value = {
		@ApiResponse(responseCode = "200", 
						description = "Returns the newly created customer",
						content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Customer.class))}),
	    @ApiResponse(responseCode = "400",
	    				description = "Invalid values for attributes or missing attributes in request body",
	    				content = @Content)
	})
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Customer> createCustomer(@Parameter(description="A new customer entity in JSON format") @RequestBody Customer newCustomer) {
		return ResponseEntity.ok(custService.createCustomer(newCustomer));
	}
	
	/**
	 * Updates an existing customer with new attribute values.
	 * @param customerId the ID of the customer.
	 * @param updateAttributes The attributes to update and their new values in map format.
	 * @return the customer with updated attributes.
	 * @throws JsonMappingException this exception is unlikely to occur but is thrown by Jackson ObjectMapper.
	 */
	@Operation(summary = "Update attributes of an existing customer")
	@ApiResponses(value = {
		@ApiResponse(responseCode = "200", 
						description = "Returns the customer with updated attributes",
						content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Customer.class))}),
	    @ApiResponse(responseCode = "400",
	    				description = "Invalid values for attributes or missing attributes in request body",
	    				content = @Content),
	    @ApiResponse(responseCode = "404",
						description = "No customer with provided ID found",
						content = @Content)
		
	})
	@RequestMapping(method = RequestMethod.PATCH, value = "/{customerId}")
	public ResponseEntity<Customer> updateCustomer(@Parameter(description="The ID of the customer") @PathVariable Long customerId, 
			@Parameter(description="The attributes to update and their new values in JSON format") @RequestBody Map<String, Object> updateAttributes) throws JsonMappingException {
		Customer custToBeUpdated = custService.getCustomerById(customerId);
		return ResponseEntity.ok(custService.updateCustomer(objectMapper.updateValue(custToBeUpdated, updateAttributes)));
	}
	
	/**
	 * Delete an existing customer.
	 * @param customerId the ID of the customer.
	 */
	@Operation(summary = "Delete the customer with the provided ID")
	@ApiResponse(responseCode = "204", 
					description = "Returns no content regardless of whether customer was successfully deleted", 
					content = @Content
	)
	@RequestMapping(value = "/{customerId}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteCustomerById(@Parameter(description="The ID of the customer") @PathVariable Long customerId) {
		try {
			custService.deleteCustomer(customerId);
		} catch (EntityNotFoundException e) {
			// This could throw a 404 instead, somewhat debated...
			return;
		}
	}
}
