package com.alinta.api.customerapi.service;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alinta.api.customerapi.model.Customer;
import com.alinta.api.customerapi.repository.CustomerRepository;

/**
 * The service that directly interacts with the customer repository.
 * @author Geoff Wang
 *
 */
@Service
public class CustomerService {

	@Autowired
	CustomerRepository custRepo;
	
	public Customer getCustomerById(Long customerId) throws EntityNotFoundException {
		Optional<Customer> cust = custRepo.findById(customerId);
		if (!cust.isPresent()) {
			throw new EntityNotFoundException("Customer with ID " + customerId.toString() + " not found.");
		}
		return cust.get();
	}
	
	public List<Customer> getCustomerByNamePartial(String partialName) {
		return custRepo.findByFirstNameContainingOrLastNameContainingAllIgnoreCase(partialName, partialName);
	}
	
	public Customer createCustomer(Customer newCustomer) {
		return custRepo.save(newCustomer);
	}
	
	public Customer updateCustomer(Customer updatedCust) {
		return custRepo.save(updatedCust);
	}
	
	public void deleteCustomer(Long customerId) throws EntityNotFoundException {
		Customer custToDelete = getCustomerById(customerId);
		custRepo.delete(custToDelete);
	}
}
