# alinta-customer-api

For Alinta coding test.

## Description

Customer API using Spring Boot framework with Spring JPA and Hibernate as the underlying ORM. Uses a sample H2 database with five existing customers.

The API can perform the following:
- Adding customers
- Editing customers
- Deleting customers
- Searching for customers by name

# Installation

## Pre-requisites

You will need Java 8 JDK, a Java 8 compatible version of Apache Maven and network access to Maven central repository.

## Build

To build this application, open a command window in the root folder and run command: `mvn clean install`

## Run

To run this application, ensure application is built, then open a command window in the root folder and run command: `java -jar target/alinta-customer-api.jar`

# Usage

## H2 Database

You can query the H2 database using the H2 console: http://localhost:8080/h2-console/

Note that on startup, this application will always rebuild the H2 database from scratch so data is not persisted once application has been shut down. The sample data can be found in src/main/resources/data.sql and this file is run on startup of the application.

## Paths

The root path of this application is http://localhost:8080/ and the customer API base path is http://localhost:8080/api/v1/customers.

A full list of end points and their details can be found in the Swagger UI: http://localhost:8080/swagger-ui.html.

## Authentication

There is no authentication for this API.
